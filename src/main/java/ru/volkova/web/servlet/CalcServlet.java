package ru.volkova.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/calc") //, loadOnStartup = 1)
public class CalcServlet extends HttpServlet {

    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        final Integer a = Integer.parseInt(req.getParameter("a"));
        final Integer b = Integer.parseInt(req.getParameter("b"));
        Integer sum = a + b;
        req.setAttribute("sum", sum);
        req.getRequestDispatcher("WEB-INF/views/sum.jsp").forward(req, resp);
    }

}
