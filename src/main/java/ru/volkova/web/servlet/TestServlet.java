package ru.volkova.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(value = "/test") //, loadOnStartup = 1)
public class TestServlet extends HttpServlet {
    
    private final List<String> data = Arrays.asList("123", "456", "789");
    
    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        req.setAttribute("data", data);
        req.getRequestDispatcher("WEB-INF/views/test.jsp").forward(req, resp);
    }

}
