<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>SUM</title>
    </head>
    <body>
        <%
            final List<String> data = (List<String>)request.getAttribute("data");
        %>
        <ul>
            <% for (final String value: data) { %>
            <li>
                <%=value%>
            </li>
            <% } %>
        </ul>
    </body>
</html>
