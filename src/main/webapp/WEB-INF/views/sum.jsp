<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>SUM</title>
    </head>
    <body>
        <h1 style="color: blue"><%= request.getAttribute("sum") %></h1>
        <h1 style="color: red">${sum}</h1>
    </body>
</html>
